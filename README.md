# Amdatu Bndtools ACE

Amdatu Bndtools ACE makes it easy to use Apache ACE during development.

## Usage
Use the Amdatu Bndtools ACE framework in your Bnd(tools) run configuration and run.

`-runfw: org.amdatu.bndtools.ace.framework;version=latest`

By default Amdatu Bndtools ACE starts an embedded server-allinone instance of  Apache Ace on port 8080. To use an existing ACE server add `org.amdatu.bndtools.ace.framework.mode=client` to the  `-runproperties` in the bndrun file.

Additional configuration options:

  - __org.amdatu.bndtools.ace.feature__ - Change the ACE feature name
  - __org.amdatu.bndtools.ace.distribution__ - Change the ACE distribution name
  - __org.amdatu.bndtools.ace.target__ - Change the ACE target name
  - __org.osgi.service.http.port__ - Change the HTTP port used for the ACE server
  - __org.apache.ace.server__ - Configure hostname / port of ACE server to use (default localhost:_org.osgi.service.http.port_ )
  - __org.apache.ace.server__ - Configure hostname / port of ACE OBR to use (default localhost:_org.osgi.service.http.port_ )

## Building

The source code can be built on the command line using Gradle, or by using [Bndtools](http://bndtools.org/) in Eclipse.
Tests can be ran from command line using Gradle as well, or by using JUnit in Eclipse.

### Eclipse using Bndtools
When Bndtools is correctly installed in Eclipse, import all subprojects into the workspace using `Import -> Existing Projects into Workspace` from the Eclipse context menu. Ensure `project -> Build Automatically` is checked in the top menu, and when no (compilation) errors are present, each subproject's bundles should be available in their `generated` folder.

###Gradle
When building from command line, invoke `./gradlew jar` from the root of the project. This will assemble all project bundles and place them in the `generated` folder of each subproject.


## Testing

### Eclipse using Bndtools
Unit tests can be ran per project or per class, using JUnit in Eclipse. Unit tests are ran by right clicking a project which contains a non-empty test folder or a test class and choosing `Run As -> JUnit Test`.

Integration tests can be ran in a similar way; right click an integration test project (commonly suffixed by .itest), but select `Run As -> Bnd OSGi Test Launcher (JUnit)` instead.

### Gradle
Unit tests are ran by invoking `./gradlew test` from the root of the project.

Integration tests can be ran by running a full build, this is done by invoking `./gradlew build` from the root of the project.
More info on available Gradle tasks can be found by invoking `./gradlew tasks`.

## Links

* [Amdatu Website]
* [Source Code]
* [Issue Tracking]
* [Continuous Build]

## License

The Amdatu Web project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).